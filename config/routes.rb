Rails.application.routes.draw do
  post "/sortable/reorder", to: "sortable#reorder"
  post "/sortable/reverse_reorder", to: "sortable#reverse_reorder"
end
